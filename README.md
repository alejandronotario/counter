# counter
Aplicación que cuenta ocurrencias de un campo a elegir de tablas procedentes de bases de datos mediante conector Spark.

```mermaid

flowchart LR
    id1(API) --> id5(Adaptador
    Primario) --> id2{{"`Puerto
    Primario
    **APP CORE**
    Puerto
    Secundario`"}} 
    id3[(BBDD)] --> id4(Adaptador
    Secundario) -->id2{{"`Puerto
    Primario
    **APP CORE**
    Puerto
    Secundario`"}}
```

## Componentes

__Core__

Contiene la lógica de la aplicación. En este caso consiste en contar registros para valores únicos de algún
campo de alguna tabla.

__Puertos__

Contiene interfaces de entrada y de salida, primarios y secundarios respectivamente.
Son interfaces que la aplicación ofrece para que los actores exteriores interactúen con esta. En este caso:

- Primario: Adquiere la consulta por parte del usuario -> puerto conductor
- Secundario: Interactúa con bases de datos -> puerto conducido 

__Adaptadores__

Contiene los adaptadores de entrada y de salida, primarios y secundarios respectivamente.
Los actores interactúna con los puertos mediante algna tecnología en concreto. Los adaptadores son
componentes que permiten esta interacción. De igual modo se incluyen adaptadores primarios o conductores
y secundarios o conducidos.

__Capas de control__

Para el control de la calidad del código y de la ETL se incluyen ficheros de configuración para la ejecución 
de Flake y Behave durante las pipelines.

Behave en in marco de desarrollo guiado por comportamiento BDD con el que se valida si la aplicación ETL cumple
con unos requisitos mínimos. Se usa Gherking y los decoradores de Python Given, When, Then.
- Given: debemos añadir todos los pasos necesarios para llegar hasta el punto donde queremos realizar el test.
- When realizamos la acción, lo que se prueba. Idealmente sería una única sentencia.
- Then: comprobamos que el resultado de la acción es el esperado.



