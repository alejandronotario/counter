"""Interfaz de aplicación."""
from __future__ import annotations
from abc import abstractmethod
from typing_extensions import Protocol
from pyspark.sql.dataframe import DataFrame


class GetTopCountingInterface(Protocol):
    
    @abstractmethod
    def get_count(self, dataframe: DataFrame, field: str) -> dict:
        raise NotImplementedError
    @abstractmethod
    def get_top(self, count_dict: dict) -> list:
        raise NotImplementedError
    