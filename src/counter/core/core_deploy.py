"""Implementación de conteo."""
from counter.core.core import GetTopCountingInterface
from pyspark.sql import functions as fn
from pyspark_llap import HIVE_WAREHOUSE_CONNECTOR, HiveWarehouseSession
from pyspark.sql.dataframe import DataFrame

class GetTopCounting(GetTopCountingInterface):
    def __init__(self) -> None:
        super().__init__()
    def get_count(self, dataframe: DataFrame, field: str) -> dict:
        """
        Método de conteo.
        - dataframe: dataframe pyspark seleccionado.
        - field: campo de dataframe de cuyo conteo 
        se requiere.
        """
        count_dict = dataframe.groupBy(field).count().select(
            field, "count").rdd.collectAsMap()        
        return count_dict
    def get_top(self, count_dict: dict, n: int=None) -> list:
        """
        Genera lista con los n top valores únicos según
        número de registros.
        """
        top_list = []
        for key, value in {k: v for k, v in sorted(
            count_dict.items(),
              key=lambda item: item[1], reverse=True)}.items():              
            top_list.append((key, value))
        return top_list[:n]
    