"""Decorador para imprimir grupo y descripción de jobs en Sparkhistory."""
from __future__ import annotations

def set_job_group(sc, group_id, description):
    def in_set_job_group(func):
        def wrapper(*args, **kwargs):
            sc.setJobGroup(group_id, description)
            result = func(*args, **kwargs)
            return result
        return wrapper
    return in_set_job_group