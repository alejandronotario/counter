"""Adaptador de fuente de datos."""
from __future__ import annotations
from counter.ports.secondary_ports.data_port import DataSource, Data
from pyspark_llap import HiveWarehouseSession
from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.dataframe import DataFrame

class GetDataSource(DataSource):

    def __init__(self, spark: SparkSession, spark_context: SparkContext,
                  session=HiveWarehouseSession) -> None:
        self._spark = spark
        self._spark_context = spark_context
        self._session = session
    
    def get_data_source(self, table=None) -> DataFrame:
        self.table = table      
        return self.table
    
class GetData(Data):
    """
    Inicia sesión en fuente de datos y ejecuta consultas.
    """

    def __init__(self, session: HiveWarehouseSession, query: str) -> None:
        self.session = session
        self.query = query

    def execute_hive_query(self) -> None:
        return self.session.sql(self.query)
   