"""Adaptador de fuente de usuario."""
from __future__ import annotations
from counter.ports.primary_ports.app_port import PostQuery


class PostDataQuery(PostQuery):
    """Se adquieren los datos de consulta del usuario."""

    def __init__(self, table: str, fields:str, field_to_count: str, n: int): 
        """argumentos.
        
        table: tabla de Cloudera que se consulta.
        fields: campos que se quieren traer.
        field_to_count: campo del que se quiere hacer el conteo.
        n: numero de valores unicos que se recogen.
        """
        self.table = table
        self.fields = fields
        self.field_to_count = field_to_count
        self.n = n
