"""Interfaz de entrada de consulta desde usuario."""
from __future__ import annotations
from typing_extensions import Protocol
from dataclasses import dataclass

@dataclass
class PostQuery(Protocol):
    """
    Clase en la que se introduce la consulta del usuario
    """
    table: str
    fields: str
    field_to_count: str
    n: int
