"""Interfaz de adquisición de datos y ejecución de consultas."""
from __future__ import annotations
from abc import abstractmethod
from typing_extensions import Protocol

class DataSource(Protocol):
    @abstractmethod
    def get_data_source(self) -> None:
        raise NotImplementedError
class Data(Protocol):
    @abstractmethod
    def execute_hive_query(self) -> None:
        raise NotImplementedError
    
    