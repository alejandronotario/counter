from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()
with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name='ejemplo',
    version='0.0.1',
    install_requires=required,
    packages=find_packages(exclude=['tests']),
    url='https://gitlab.com/alejandronotario/counter',
    author='AN',
    author_email='alejandronotario@gmail.es',
    license='',
    description='ejemplo etl de contar registros',

)