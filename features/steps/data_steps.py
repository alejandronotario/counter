from behave import given, when, then

@given('Una lista de enteros "{lista}"')
def step_impl(context, lista):
    context.lista = lista

@when('Contando elementos')
def step_impl(context):    
    context.result = len(context.lista)

@then("La lista no esta vacia")
def step_impl(context):
    assert context.result > 0
