from flask import Flask, jsonify, request
import json
from pyspark_llap import HiveWarehouseSession
from pyspark import SparkContext
from pyspark.sql import SparkSession
from counter.adapters.primary_adapters.app_adapter import PostDataQuery
from counter.adapters.secondary_adapters.data_adapter import GetData
from counter.core.core_deploy import GetTopCounting
from counter.utils.utils import set_job_group

spark = SparkSession.builder.enableHiveSupport().getOrCreate()
sc = SparkContext.getOrCreate()
hive = HiveWarehouseSession.session(spark).build()

app = Flask(__name__)

@app.route('/', methods=['GET'])
def deploy_executor():
    result = "result"
    return jsonify(result)
@set_job_group(sc=sc, group_id="grupo_etl_API", description="contador de registros_API")
@app.route("/get_user_query", methods=['GET', 'POST'])
def get_user_query():
    table = json.loads(request.args["table"])
    fields = json.loads(request.args["fields"])
    field_to_count = json.loads(request.args["field_to_count"])
    n = json.loads(request.args["n"])
    p = PostDataQuery(table=table,
                       fields=fields,
                         field_to_count=field_to_count,
                           n=n)
    l = GetTopCounting().get_top(
        count_dict=GetTopCounting().get_count(
            dataframe=GetData(
                session=hive,
                  query="select {0} from {1}".format(p.fields, p.table)
            ).execute_hive_query(), field=p.field_to_count
        ), n=p.n
    )
    return jsonify(l) 


if __name__ == '__main__':
    app.run()
