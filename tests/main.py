from counter.adapters.secondary_adapters.data_adapter import GetData
from counter.core.core_deploy import GetTopCounting
from counter.adapters.primary_adapters.app_adapter import PostDataQuery
from pyspark_llap import HiveWarehouseSession
from pyspark import SparkContext
from pyspark.sql import SparkSession
from counter.utils.utils import set_job_group

spark = SparkSession.builder.enableHiveSupport().getOrCreate()
sc = SparkContext.getOrCreate()
hive = HiveWarehouseSession.session(spark).build()

@set_job_group(sc=sc, group_id="grupo_etl", description="contador de registros")
def main():
       p = PostDataQuery(table="table",
                       fields="field1, field2",
                         field_to_count="tfield2", n=2)
       l = GetTopCounting().get_top(
            count_dict=GetTopCounting().get_count(
                dataframe=GetData(
                    session=hive,
                    query="select {0} from {1}".format(p.fields, p.table)
                ).execute_hive_query(), field=p.field_to_count
            ), n=p.n
        )
       print(l)

if __name__ == "__main__":
       main()

    



